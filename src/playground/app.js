class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);
    this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    this.handlePick = this.handlePick.bind(this)
    this.handleAddOption = this.handleAddOption.bind(this)
    this.handleDeleteOption = this.handleDeleteOption.bind(this)
    this.state = {
      options: props.options

    }

  }

  componentDidMount() {

    try {
      const options = JSON.parse(localStorage.getItem('options'));

      if (options) {
        this.setState(() => ({
          options: options
        }))
      }
    }
    catch (e) {

    }
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
      console.log('saving Data');
    }
  }

  componentWillUnmount() {
    console.log('ComponentWillUnmount')
  }

  //handleDeleteOption
  handleDeleteOptions(e) {
    this.setState(() => ({ options: [] }))
  }

  handlePick() {
    const options = this.state.options;
    const random = Math.floor(Math.random() * options.length);
    alert(options[random]);
  }
  //handlePick --pass down to Aciton and setup Onclick -bind here 
  //randomly pick and option and alret it 

  handleAddOption(option) {
    if (!option) {
      return 'Enter valid value to add item'
    }
    else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists'
    } else {
      this.setState((prevState) => ({
        options: prevState.options.concat(option)
      }))

    }
  }

  handleDeleteOption(optionKey) {
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => optionKey !== option)

    }))

  }

  render() {

    const subtitle = "Put your life in the hands of the Computer";
    return (
      <div>
        <Header subtitle={subtitle} />
        <Action hasOptions={this.state.options.length > 0}
          handlePick={this.handlePick}
        />
        <Options options={this.state.options}
          handleDeleteOptions={this.handleDeleteOptions}
          handleDeleteOption={this.handleDeleteOption} />
        <ActionSubmit
          handleAddOption={this.handleAddOption}
        />
      </div>
    );
  }
}


IndecisionApp.defaultProps = {
  options: []
}


const Header = (props) => {
  return (
    <div>
      <h1>{props.title}</h1>
      {props.subtitle && <h2>{props.subtitle}</h2>}
    </div>
  );
}

Header.defaultProps = {
  title: 'Indecision App'
}


const Action = (props) => {
  return (
    <div>
      <button
        onClick={props.handlePick}
        disabled={!props.hasOptions}
      >
        What Should I do?
      </button>
    </div>
  );
}

//option  Coment List of current avaiable options
//AddOption component -> Submit
//Add Remove all button
//Setup HandleRemove All -> alert Some message

const Options = (props) => {
  return (

    <div>
      <button onClick={props.handleDeleteOptions}>Remove all</button>
      <h4>  {props.options.length === 0 && <p>Please add an  option to get started!</p>}</h4>

      <ol>
        {props.options.map((option) => (
          <Option
            key={option}
            optionText={option}
            handleDeleteOption={props.handleDeleteOption}
          />
        ))}
      </ol>
    </div>
  );

}

const Option = (props) => {

  return (
    <div>
      <li> {props.optionText}
        <button onClick={(e) => {
          props.handleDeleteOption(props.optionText)
        }}

        >Remove</button>
      </li>
    </div >
  );
}

class ActionSubmit extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined,
      name: 'Suman'
    }
  }

  handleAddOption(e) {
    e.preventDefault();
    const option = e.target.elements.option.value.trim();
    const error1 = this.props.handleAddOption(option);
    e.target.elements.option.value = ''
    this.setState(() => ({ error: error1 }))

    if (!error) {
      e.targer.elements.option.value = '';
    }
  }

  render() {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.handleAddOption} id='add-ption'>
          <input
            type="text"
            placeholder="Add what you want to do?"
            name="option"
          ></input>
          <button>Add option </button>
        </form>
      </div>

    );
  }
}
ReactDOM.render(<IndecisionApp />, document.querySelector("#app"));
