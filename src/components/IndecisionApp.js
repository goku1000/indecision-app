import React from 'react'

import Header from './Header'
import AddOption from './AddOption'
import Options from './Options'
import Action from './Action'

import OptionModal from './OptionModal'


export default class IndecisionApp extends React.Component {

    state = {
        options: [],
        selectedOption: false
    }

    componentDidMount() {

        try {
            const options = JSON.parse(localStorage.getItem('options'));

            if (options) {
                this.setState(() => ({
                    options: options
                }))
            }
        }
        catch (e) {

        }
    }

    componentDidUpdate(prevProp, prevState) {
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
            console.log('saving Data');
        }
    }

    componentWillUnmount() {
        console.log('ComponentWillUnmount')
    }

    //handleDeleteOption
    handleDeleteOptions = (e) => {
        this.setState(() => ({ options: [] }))
    }

    handlePick = () => {
        const options = this.state.options;
        const random = Math.floor(Math.random() * options.length);
        const option = options[random];
        console.log(option)
        this.setState(() => ({
            selectedOption: option
        }))
        console.log(this.state);
    }
    //handlePick --pass down to Aciton and setup Onclick -bind here 
    //randomly pick and option and alret it 

    handleAddOption = (option) => {
        if (!option) {
            return 'Enter valid value to add item'
        }
        else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists'
        } else {
            this.setState((prevState) => ({
                options: prevState.options.concat(option)
            }))

        }
    }

    handleDeleteOption = (optionKey) => {
        this.setState((prevState) => ({
            options: prevState.options.filter((option) => optionKey !== option)

        }))

    }

    handleClearSelectedOption = () => {
        this.setState(() => ({
            selectedOption: undefined
        }))
    }

    render() {

        const subtitle = "Put your life in the hands of the Computer";
        return (
            <div>
                <Header subtitle={subtitle} />
                <div className='container'>
                    <Action hasOptions={this.state.options.length > 0}
                        handlePick={this.handlePick}
                    />
                    <div className="widget">
                        <Options options={this.state.options}
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleDeleteOption={this.handleDeleteOption} />
                        <AddOption
                            handleAddOption={this.handleAddOption}
                        />
                    </div>
                </div>
                <OptionModal
                    selectedOption={this.state.selectedOption}
                    handleClearSelectedOption={this.handleClearSelectedOption}
                />
            </div>
        );
    }
}


IndecisionApp.defaultProps = {
    options: []
}
