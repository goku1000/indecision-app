import React from 'react';


export default class AddOption extends React.Component {
    state = {
        error: undefined
    }


    handleAddOption = (e) => {
        e.preventDefault();
        const option = e.target.elements.option.value.trim();
        const error1 = this.props.handleAddOption(option);
        e.target.elements.option.value = ''
        this.setState(() => ({ error: error1 }))

        if (!error1) {
            e.target.elements.option.value = '';
        }
    }

    render() {
        return (
            <div>
                {this.state.error && <p className="add-option-error">{this.state.error}</p>}
                <form className="add-option"
                    onSubmit={this.handleAddOption} id='add-otion'>
                    <input
                        className="add-option__input"
                        type="text"
                        placeholder="Add what you want to do?"
                        name="option"
                    ></input>
                    <button className="button">Add option </button>
                </form>
            </div>

        );
    }
}