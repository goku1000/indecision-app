import React from 'react'
import Option from './Option'

const Options = (props) => (
    <div>
        <div className="widget-header">
            <h3 className="widget-header__title">Your Options</h3>
            <button
                onClick={props.handleDeleteOptions}
                className="button button--link"
            >Remove all</button>
        </div>


        <h4>  {props.options.length === 0 &&
            <p className="widget__message">Please add an  option to get started!</p>}</h4>

        <ol>
            {props.options.map((option) => (
                <Option
                    key={option}
                    optionText={option}
                    handleDeleteOption={props.handleDeleteOption}
                />
            ))}
        </ol>
    </div>

)




export default Options;